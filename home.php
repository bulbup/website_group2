
<!-- chua tat ca noi dung trang home-->
<div class="container-fluid home">
<!--content1-->
	<!-- chua phan thu 1 cua HOME-->
	<div class="content1  ">
		<!-- anh nen phia tren -->
		<div class="content1-img">
			<img src="image/global.jpg" class="img-responsive">
		</div><!-- end anh nen-->

		<!-- noi dung cac 3 the-->
		<div class="content1-group-card container">
		<!--nd 1 the-->
			<div class="content1-card col-md-4 text-center">
				<div class="content1-card-icon">
					<img src="image/icon_1.png">
				</div>
				<div class="content1-card-text">
					<h3>DEDICATED AGILE RESOURCES</h3>
					<p>
						Quân vương độc tấu khúc tiêu sầu <br>
						sông núi ngàn trùng có thấy đâu <br>
						Tri kỷ hồng nhan không giữ nổi <br>
						giang sơn gấm vóc biết là giàu <br>
						tiêu ngân Khắc khoải hờn duyên số <br>
						trúc vọng tâm tình mãi nhớ nhau <br>
						vớ vẩn tràng giang làng sống gợn <br>
						thẩn thờ tấu khúc cả trời đau! <br>
					</p>
				</div>
			</div><!--end 1 the-->

			<!--nd 1 the-->
			<div class="content1-card col-md-4 text-center">
				<div class="content1-card-icon">
					<img src="image/icon_1.png">
				</div>
				<div class="content1-card-text">
					<h3>DEDICATED AGILE RESOURCES</h3>
					<p>
						Quân vương độc tấu khúc tiêu sầu <br>
						sông núi ngàn trùng có thấy đâu <br>
						Tri kỷ hồng nhan không giữ nổi <br>
						giang sơn gấm vóc biết là giàu <br>
						tiêu ngân Khắc khoải hờn duyên số <br>
						trúc vọng tâm tình mãi nhớ nhau <br>
						vớ vẩn tràng giang làng sống gợn <br>
						thẩn thờ tấu khúc cả trời đau! <br>
					</p>
				</div>
			</div><!--end 1 the-->

			<!--nd 1 the-->
			<div class="content1-card col-md-4 text-center">
				<div class="content1-card-icon">
					<img src="image/icon_1.png">
				</div>
				<div class="content1-card-text">
					<h3>DEDICATED AGILE RESOURCES</h3>
					<p>
						Quân vương độc tấu khúc tiêu sầu <br>
						sông núi ngàn trùng có thấy đâu <br>
						Tri kỷ hồng nhan không giữ nổi <br>
						giang sơn gấm vóc biết là giàu <br>
						tiêu ngân Khắc khoải hờn duyên số <br>
						trúc vọng tâm tình mãi nhớ nhau <br>
						vớ vẩn tràng giang làng sống gợn <br>
						thẩn thờ tấu khúc cả trời đau! <br>
					</p>
				</div>
			</div><!--end 1 the-->

		</div><!-- end 3 the-->
		
	</div><!-- end content1-->
	
<!--content2-->
	<div class="content2  row">
	<!--content-title-->
		<div class="col-md-12 content-title text-center">
			<h2 style="color: #F3280D">OUR SERVICES</h2>
		</div><!--end content-title-->

		<!--content2-group-card-->
		<div class="content2-group-card container">
		<!--1card-->
			<div class="content2-card text-center col-md-6">
				<div class="content2-card-icon">
					<img src="image/dm_icon_3.png">
				</div>
				<div class="content2-card-text">
					<h3 style="color: #0994BC">WEBSITE DEVELOPMENT</h3>
					<p>
						Khẽ đưa ngón ngọc vuốt bi ai <br>
						Vẽ tình muôn thủa chẳng riêng mình<br>
						Dây tranh thánh thót lòng xao động<br>
						Ngọc tiêu bay vút, tâm như bẫng tâm như không.<br>
						Ta tạm gác bụi hồng trần nhân thế.<br>
						Gửi nỗi lòng vào giây phút thiên thai.﻿<br>
					</p>
				</div>
			</div><!--end 1 card-->

			<!--1card-->
			<div class="content2-card text-center col-md-6">
				<div class="content2-card-icon">
					<img src="image/dm_icon_3.png">
				</div>
				<div class="content2-card-text">
					<h3 style="color: #0994BC">SMART DEVICE (MOBILE) SOLUTION</h3>
					<p>
						Khẽ đưa ngón ngọc vuốt bi ai <br>
						Vẽ tình muôn thủa chẳng riêng mình<br>
						Dây tranh thánh thót lòng xao động<br>
						Ngọc tiêu bay vút, tâm như bẫng tâm như không.<br>
						Ta tạm gác bụi hồng trần nhân thế.<br>
						Gửi nỗi lòng vào giây phút thiên thai.﻿<br>
					</p>
				</div>
			</div><!--end 1 card-->

			<!--1card-->
			<div class="content2-card text-center col-md-6">
				<div class="content2-card-icon">
					<img src="image/dm_icon_3.png">
				</div>
				<div class="content2-card-text">
					<h3 style="color: #0994BC">COMMERCE SYSTEM CONSTRUCTION</h3>
					<p>
						Khẽ đưa ngón ngọc vuốt bi ai <br>
						Vẽ tình muôn thủa chẳng riêng mình<br>
						Dây tranh thánh thót lòng xao động<br>
						Ngọc tiêu bay vút, tâm như bẫng tâm như không.<br>
						Ta tạm gác bụi hồng trần nhân thế.<br>
						Gửi nỗi lòng vào giây phút thiên thai.﻿<br>
					</p>
				</div>
			</div><!--end 1 card-->

			<!--1card-->
			<div class="content2-card text-center col-md-6">
				<div class="content2-card-icon">
					<img src="image/dm_icon_3.png">
				</div>
				<div class="content2-card-text">
					<h3 style="color: #0994BC">TESTING</h3>
					<p>
						Khẽ đưa ngón ngọc vuốt bi ai <br>
						Vẽ tình muôn thủa chẳng riêng mình<br>
						Dây tranh thánh thót lòng xao động<br>
						Ngọc tiêu bay vút, tâm như bẫng tâm như không.<br>
						Ta tạm gác bụi hồng trần nhân thế.<br>
						Gửi nỗi lòng vào giây phút thiên thai.﻿<br>
					</p>
				</div>
			</div><!--end 1 card-->
		</div><!--end content2-group-card-->


	</div><!--end content2-->


	<!--content2-->
	<div class="content3">
		<!--content-title-->
		<div class="col-md-12 content-title text-center">
			<h2 style="color: white;">LEADERSHIP</h2>			
		</div><!--end content-title-->
		<div class="container content3-group-card">
		<!-- 1 card -->
			
				<div class="col-md-6 content3-card">
				<!-- icon -->
					<div class="content3-card-icon">
						<img src="image/aboutus_leadership_images1.jpg" class="bder">
					</div><!-- icon -->
				<!--card text-->
					
					<div class="bder">
						<div class="content3-card-text">
							<h3 style="color: #19A4CB">Takashi Shimizu</h3>
							<small>Chief Executive Officer</small>
							<p class="text-center"><br>
								Khẽ đưa ngón ngọc vuốt bi ai <br>
								Vẽ tình muôn thủa chẳng riêng mình<br>
								Dây tranh thánh thót lòng xao động<br>
								Ngọc tiêu bay vút, tâm như bẫng tâm như không.<br>
								Ta tạm gác bụi hồng trần nhân thế.<br>
								Gửi nỗi lòng vào giây phút thiên thai.﻿<br>
							</p>
						</div><!--card text-->
					</div>
					
			</div><!-- 1 card -->
			<!-- 1 card -->
			
				<div class="col-md-6 content3-card">
				<!-- icon -->
					<div class="content3-card-icon">
						<img src="image/aboutus_leadership_images2_0.jpg" class="bder">
					</div><!-- icon -->
				<!--card text-->
					
					<div class="bder">
						<div class="content3-card-text ">
							<h3 style="color: #19A4CB">Vincete Segarra Larrazabal</h3>
							<small>Chief Operation Officer</small>
							<p class="text-center"><br>
								Khẽ đưa ngón ngọc vuốt bi ai <br>
								Vẽ tình muôn thủa chẳng riêng mình<br>
								Dây tranh thánh thót lòng xao động<br>
								Ngọc tiêu bay vút, tâm như bẫng tâm như không.<br>
								Ta tạm gác bụi hồng trần nhân thế.<br>
								Gửi nỗi lòng vào giây phút thiên thai.﻿<br>
							</p>
						</div><!--card text-->
					</div>
					
			</div><!-- 1 card -->
		</div>
	</div>
	<!--content2-->



</div><!-- end trang home-