<?php include_once 'header.php'; ?> 
<!-- <link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css"> -->
<div class="row wrapper">
	<div class="col-md-12">
		<div class="panel-default">
			<div class="panel-heading">
				
				<img src="image/Tech.png" style="width: 100%;" />
				
			</div>
			<div class ="row">
			<div class ="main_TT">
				<b>TECHNOLOGIES</b>
			</div>
				<div class ="col-md-2 col-md-offset-3">
					<div class="title">
						DATABASE
					</div>
					<ul><br>
						<li>mySQL</li><br>
						<li>Oracle</li><br>
						<li>Mongo DB</li><br>
						<li>Postgre SQL</li><br>
						<li>Orient DB</li><br>
						<li>SQLite</li>
					</ul><br><br>
					<div class="title">
						CMS
					</div>
					<ul><br>
						<li>Drupal</li><br>
						<li>HeartCore</li>
					</ul>
				</div>
				<div class ="col-md-2">
					<div class="title">
						LANGUAGES
					</div>
					<ul><br>
						<li>Java</li><br>
						<li>PHP</li><br>
						<li>C#</li><br>
						<li>Android</li><br>
						<li>Objecttive-C</li><br>
						<li>HTML5/JavaCsript</li>
					</ul><br><br>
					<div class="title">
						CRM
					</div>
					<ul><br>
						<li>Salesforce</li>
					</ul>
				</div>
				<div class ="col-md-2">
					<div class="title">
						FRAMEWORKS
					</div>
					<ul><br>
						<li>Spring</li><br>
						<li>Tabestry</li><br>
						<li>Play</li><br>
						<li>Laravel</li><br>
						<li>Symfony</li><br><br>
					</ul><br><br>
					<div class="title">
						AUTOMATION
					</div>
					<ul><br>
						<li>Jenkins</li><br>
						<li>Maven</li><br>
						<li>Ant</li><br>
						<li>Grunt</li>
					</ul>
				</div>
				<div class ="col-md-2">
					<div class="title">
						E-COMMECRE
					</div>
					<ul><br>
						<li>Magento</li><br>
						<li>Ec-Cube</li><br><br><br><br><br><br><br><br>
					</ul><br><br>
					<div class="title">
						MANAGEMENT
					</div>
					<ul><br>
						<li>Rebmine</li><br>
						<li>ALMinium</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<br><br><br>	
<?php include_once 'footer.php' ?>